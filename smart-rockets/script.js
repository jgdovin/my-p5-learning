"use strict";
var population;
var lifespan = 400;
var lifeP;
var count = 0;
var target;
var rectCoords = [100, 100, 200, 10];
var maxForce = 0.2;
function setup() {
    createCanvas(400, 350);
    population = new Population();
    lifeP = createP();
    target = createVector(width/2, 50);
}

function draw() {
    background(0);
    population.run();
    lifeP.html(count);
    count++;

    if (count === lifespan) {
        population.evaluate();
        population.selection();
        count = 0;
       
    }

    rect(rectCoords[0], rectCoords[1], rectCoords[2], rectCoords[3]);
    ellipse(target.x, target.y, 16, 16);
}

function Population() {
    var parentPop = this;
    this.rockets = [];
    this.popSize = 100;
    this.matingPool = [];
    for (var i = 0; i < this.popSize; i++) {
        this.rockets[i] = new Rocket();
    }

    this.evaluate = function() {
        var that = this;
        this.maxfit = 0;

        this.rockets.forEach(function(rocket) {
            rocket.calcFitness();
            if (rocket.fitness > that.maxfit) {
                that.maxfit = rocket.fitness;
            }
        });

        this.rockets.forEach(function(rocket) {
            rocket.fitness /= that.maxfit;
        });

        this.matingPool = [];
        this.rockets.forEach(function(rocket) {
            var n = rocket.fitness * 100;
            for (var j = 0; j < n; j++) {
                that.matingPool.push(rocket);
            }
        });


    }

    this.selection = function() {
        var newRockets = [];
        for (var i = 0; i < this.rockets.length; i++) {
            var parentA = random(this.matingPool).dna;
            var parentB = random(this.matingPool).dna;
            
            var child = parentA.crossover(parentB);
            child.mutation();
            newRockets[i] = new Rocket(child);
        }
        this.rockets = newRockets;
    }

    this.run = function() {
        this.rockets.forEach(function(rocket) {
            rocket.update();
            rocket.show();
        });
    }
}

function DNA(genes) {
    if (genes) {
        this.genes = genes;
    } else {
        this.genes = [];
        for (var i = 0; i < lifespan; i++) {
            this.genes[i] = p5.Vector.random2D();
            this.genes[i].setMag(maxForce);
        }
    }
    
    this.crossover = function(partner) {
        var newgenes = [];
        var mid = floor(random(this.genes.length));

        for (var i = 0; i < this.genes.length; i++) {
            if (i > mid) {
                newgenes[i] = this.genes[i];
            } else {
                newgenes[i] = partner.genes[i];
            }
        }
        return new DNA(newgenes);
    }

    this.mutation = function() {
        for (var i = 0; i < this.genes.length; i++) {
            if (random(1) < 0.01) {
                this.genes[i] = p5.Vector.random2D();
                this.genes[i].setMag(maxForce);
            }
        }
    }
}

function Rocket(dna) {
    this.pos = createVector(width/2, height);
    this.vel = createVector();
    this.acc = createVector();
    this.dna = dna || new DNA();
    this.fitness = 0;
    this.completed = false;
    this.crashed = false;

    this.applyForce = function(force) {
        this.acc.add(force);
    }

    this.calcFitness = function() {
        var d = dist(this.pos.x, this.pos.y, target.x, target.y);

        this.fitness = map(d, 0, width, width, 0);

        if (this.completed) {
            this.fitness *= 10;
        }

        if (this.crashed) {
            this.fitness /= 100;
        }

    }

    this.update = function() {

        var d = dist(this.pos.x, this.pos.y, target.x, target.y);

        if (d < 5) {
            this.completed = true;
            
        }

        if (this.pos.y < rectCoords[1] + rectCoords[3] && this.pos.y > rectCoords[0] && this.pos.x < rectCoords[0] + rectCoords[2] && this.pos.x > rectCoords[0]) {
            this.crashed = true;
        }

        if (this.pos.y > height || this.pos.y < 0 || this.pos.x > width || this.pos.x < 0) {
            this.crashed = true;
        }

        if (!this.completed && !this.crashed) {
            this.applyForce(this.dna.genes[count]);
            this.vel.add(this.acc);
            this.pos.add(this.vel);
            this.acc.mult(0);
        }

        
        
    }

    this.show = function() {
        push();
        noStroke();
        fill(255,150);
        translate(this.pos.x, this.pos.y)
        rotate(this.vel.heading());
        rectMode(CENTER);
        rect(0, 0, 25, 5);
        pop();
    }

}