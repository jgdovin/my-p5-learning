function Cell(x, y, size) {
    var that = this;
    this.x = x;
    this.y = y;
    this.size = size;
    this.isActive = false;
    this.xPos = this.x*size;
    this.yPos = this.y*size;
    this.walls = [true, true, true, true];
    this.visited = false;

    this.show = function() {
        stroke(255);
        this.walls.forEach(function(wall, key) {
            if(wall) {
                that.drawWall[key]();                
            }
        })
 
        if(this.visited) {
            noStroke();
            fill(100, 250, 140, 100);
            rect(this.xPos, this.yPos, this.size, this.size);
        }       
        
        
    };

    this.highlight = function() {
        fill(200, 100, 240);
        rect(this.xPos, this.yPos, this.size, this.size);
    }

    this.drawWall = [
        function() {
            line(that.xPos, that.yPos, that.xPos + that.size, that.yPos);        
        },
        function() {
            line(that.xPos + that.size, that.yPos, that.xPos + that.size, that.yPos + that.size);        
        },
        function() {
            line(that.xPos, that.yPos + that.size, that.xPos + that.size, that.yPos + that.size);
        },
        function() {
            line(that.xPos, that.yPos, that.xPos, that.yPos + that.size);
        }

    ];

    this.checkNeighbors = function() {
        var neighbors = [
            grid[index(that.x, that.y - 1)],
            grid[index(that.x + 1, that.y)],
            grid[index(that.x, that.y +1)],
            grid[index(that.x - 1, that.y)]
        ];

        for (i = neighbors.length - 1; i >= 0; i--) {
            if (!neighbors[i] || neighbors[i].visited) {
                neighbors.splice(i, 1);
            }
        }
        // neighbors.forEach(function(neighbor, key) {
            
        //     if (!neighbor || neighbor.visited) {
        //         console.log(neighbors);
        //         neighbors.splice(key, 1);
        //         console.log(neighbors);
        //     }
        // });

        
        if(neighbors.length > 0) {
            return neighbors[floor(random(0, neighbors.length))];
        }
        return;
    }
}

