function Stack() {
    var internalStack = [];

    this.put = function(item) {
        internalStack.push(item);
    }
    this.fetch = function() {
        return internalStack.pop();
    }
    this.hasItems = function() {
        return internalStack.length;
    }
}