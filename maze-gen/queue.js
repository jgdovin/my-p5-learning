function Queue() {
    var internalStack = [];

    this.put = function(item) {
        internalStack.push(item);
    }
    this.fetch = function() {
        return internalStack.shift();
    }
    this.hasItems = function() {
        return internalStack.length;
    }
}