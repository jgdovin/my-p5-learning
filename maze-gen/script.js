let w = 30;
var cols, rows;
var grid = [];
var stack = new Stack();
var activeCell;
function setup() {
    createCanvas(401, 401);
    cols = floor(width/w);
    rows = floor(height/w);

    for (col = 0; col < cols; col++) {
        for (row = 0; row < rows; row++) {
            var cell = new Cell(row, col, w);
            grid.push(cell);
        }
    }
    activeCell = grid[0];
}

function index(col, row) {
    if (col < 0 || row < 0 || col > cols - 1|| row > rows - 1) {
        return -1;
    }
    return col + row * cols;
}

function removeWalls(a, b) {
    var x = a.x - b.x;
    var y = a.y - b.y;
    if (x === 1) {
        a.walls[3] = false;
        b.walls[1] = false;
    }
    if (x === -1) {
        a.walls[1] = false;
        b.walls[3] = false;
    }
    if (y === 1) {
        a.walls[0] = false;
        b.walls[2] = false;
    }
    if (y === -1) {
        a.walls[2] = false;
        b.walls[0] = false;
    }
}

function draw() {
    frameRate(30);
    background(51);
    for (i=0; i < grid.length; i++) {
        grid[i].show();
    }
    activeCell.visited = true;
    activeCell.highlight();    
    var neighbor = activeCell.checkNeighbors();
    if (neighbor) {
        neighbor.visited = true;
        stack.put(activeCell);
        removeWalls(activeCell, neighbor);
        activeCell = neighbor;   
    } else if (stack.hasItems() > 0) {
        activeCell = stack.fetch();
    }
    
}